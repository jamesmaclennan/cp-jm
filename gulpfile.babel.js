'use strict'

const gulp = require('gulp');
const babelify = require('babelify');
const browserify = require('browserify')
const sass = require('gulp-sass');
const less = require('gulp-less');
const path = require('path');
const pug = require('gulp-pug');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const data = require('gulp-data');


const packageJSON = require('./package.json');
const vendors = Object.keys(packageJSON.dependencies);

gulp.task('js-vendor', () => {
	console.log('bundling: ', vendors)
	return browserify()
    .require(vendors)
    .bundle()
    .pipe(source('vendor.bundle.js'))
    .pipe(gulp.dest('build/js'));
})

gulp.task('js-main', () => {
	browserify('src/js/main.js')
		.external(vendors)
		.transform('babelify', {
			presets: ['env', 'react'],
			plugins: ['transform-object-rest-spread']
		})
		.bundle()	
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

// gulp.task('scss', () => {
// 	gulp.src('src/sass/main.scss')
// 		.pipe(sass().on('error', sass.logError))
// 		.pipe(gulp.dest('build/css'))
// 		.pipe(browserSync.stream());
// })

gulp.task('less', () => {
	return gulp.src('src/less/main.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./build/css'));
})

gulp.task('pug', function buildHTML() {
  return gulp.src('src/pug/*.pug')
  .pipe(pug({
		// locals: {
		// 	"site": siteJSON,
		// 	"content": contentJSON
		// },
		verbose: true
	}))
	.pipe(gulp.dest('build/'))
});

gulp.task('fonts', () => {
	gulp.src('src/fonts/**/*')
	.pipe(gulp.dest('build/fonts'))
})

gulp.task('build', ['pug', 'js-vendor', 'js-main', 'less', 'fonts']);

gulp.task('default', ['js-main', 'js-vendor', 'pug', 'fonts'],() => {
	browserSync.init({
		server: "./build"
	});
	gulp.watch("src/less/**/*", ['less']);
	gulp.watch('src/**/*',['js-main', 'pug']).on('change', browserSync.reload)
});
